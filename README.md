## DENUNCIE

Este repositorio contiene el codigo fuente de la app para denunciar abusos de los choferes.


#### Tecnologías

  * [NodeJS](https://nodejs.org/en/)
  * [Grunt](http://gruntjs.com/)
  * [Compass](http://compass-style.org/)
  * [jQuery](https://jquery.com/)
  * [Swig](http://paularmstrong.github.io/swig/)
  * [Materialize](http://materializecss.com/)

#### Requisitos

  * Obvio tener un navegador decente.
  * Tener instalado `nodejs`
  * Tener instalado grunt `[sudo] npm install -g grunt-cli`

#### Instalación

  * `git clone git@bitbucket.org:alex_condori/denuncie.git`
  * `cd denuncie`
  * `[sudo] npm install`
  * `npm install -g grunt-cli`
  * `grunt start`


### Licencia

El contenido de este proyecto y el proyecto en si mismo estan bajo la licencia [Creative Commons Attribution 3.0]
(https://creativecommons.org/licenses/by/3.0/),y todo el código fuente esta bajo la licencia 
[MIT](https://opensource.org/licenses/MIT).